import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { NavigationModule } from '@ux-aspects/ux-aspects';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NavigationModule
    ],
    declarations: [
        SideNavigationComponent
    ],
    exports: [
        SideNavigationComponent
    ]
})
export class SharedModule { }
