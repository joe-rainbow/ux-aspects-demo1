import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ApplicationsComponent } from './applications.component';
import { ChartsComponent } from './charts/charts.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
    {
        path: '',
        component: ApplicationsComponent,
        children: [
            {
                path: '',
                redirectTo: 'charts'
            },
            {
                path: 'charts',
                component: ChartsComponent
            },
            {
                path: 'list',
                component: ListComponent
            },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes),
        ChartsModule
    ],
    declarations: [ChartsComponent, ListComponent, ApplicationsComponent]
})
export class ApplicationsModule { }
