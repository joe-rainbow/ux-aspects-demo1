import { Component } from '@angular/core';
import { NavigationItem } from '@ux-aspects/ux-aspects';

@Component({
    selector: 'app-applications',
    templateUrl: './applications.component.html',
    styleUrls: ['./applications.component.less']
})
export class ApplicationsComponent {

    navigation: NavigationItem[] = [
        {
            title: 'Charts',
            icon: 'hpe-analytics',
            routerLink: 'applications/charts'
        },
        {
            title: 'List',
            icon: 'hpe-list',
            routerLink: 'applications/list'
        }
    ];

}
