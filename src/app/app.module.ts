import { HashLocationStrategy, LocationStrategy, CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { downgradeComponent, setAngularJSGlobal, UpgradeModule } from '@angular/upgrade/static';
import { ColorService, colorSets, PageHeaderModule } from '@ux-aspects/ux-aspects';
import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts';

const routes: Routes = [
    {
        path: 'administration',
        loadChildren: './administration/administration.module#AdministrationModule'
    },
    {
        path: 'applications',
        loadChildren: './applications/applications.module#ApplicationsModule'
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'applications'
    }
];

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        PageHeaderModule,
        UpgradeModule,
        ChartsModule,
        CommonModule
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        {
            provide: '$rootScope',
            useFactory: (injector: Injector) => injector.get('$rootScope'),
            deps: ['$injector']
        }
    ],
    entryComponents: [AppComponent]
})
export class AppModule {
    constructor(colorService: ColorService, private _upgrade: UpgradeModule) {
        colorService.setColorSet(colorSets.microFocus);
    }

    ngDoBootstrap() {
        this._upgrade.bootstrap(document.body, ['app'], { strictDi: true });
    }
}

/*
    AngularJS Module
*/
setAngularJSGlobal(angular);

angular
    .module('app', ['ux-aspects'])
    .directive('appRoot', downgradeComponent({ component: AppComponent }) as ng.IDirectiveFactory);
