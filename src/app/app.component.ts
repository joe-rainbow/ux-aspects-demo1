import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PageHeaderIconMenu, PageHeaderNavigationItem } from '@ux-aspects/ux-aspects';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {

    productAcronym = 'UX';
    familyColorName = 'Forest Green';

    items: PageHeaderNavigationItem[] = [
        {
            title: 'Apps',
            routerLink: 'applications'
        },
        {
            title: 'Administration',
            children: [
                {
                    title: 'Overview',
                    routerLink: 'administration/overview'
                },
                {
                    title: 'Features',
                    routerLink: 'administration/features'
                }
            ]
        }
    ];

    iconMenus: PageHeaderIconMenu[] = [
        {
            icon: 'hpe-help-circle hpe-lg',
            label: 'Help',
            dropdown: [
                {
                    title: 'About...'
                }
            ]
        }
    ];

    constructor(router: Router) {

        // perform initial navigation - required in a hybrid application
        router.initialNavigation();
    }
}
