/* SystemJS module definition */
declare var module: NodeModule;
declare var angular: ng.IAngularStatic;

interface NodeModule {
    id: string;
}
